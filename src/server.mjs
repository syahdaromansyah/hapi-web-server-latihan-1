import process from 'process';
import Hapi from '@hapi/hapi';
import routes from './modules/routes.mjs';

const initHapiServer = async () => {
  const hapiServer = Hapi.server({
    port: 5000,
    host: process.env.NODE_ENV !== 'production' ? 'localhost' : '0.0.0.0',
    routes: {
      cors: {
        origin: ['http://notesapp-v1.dicodingacademy.com'],
      },
    },
  });

  hapiServer.route(routes);

  await hapiServer.start();
  console.info(`Server running on ${hapiServer.info.uri}`);
};

initHapiServer();
