import { nanoid } from 'nanoid';
import notes from '../../database/notes.mjs';

const addNote = (request, h) => {
  const { title, tags, body } = request.payload;
  const id = nanoid(22);
  const createdAt = new Date().toISOString();
  const updatedAt = createdAt;

  const newNote = {
    title,
    tags,
    body,
    id,
    createdAt,
    updatedAt,
  };

  notes.push(newNote);

  const isSuccess = notes.filter((note) => note.id === id).length > 0;

  if (isSuccess) {
    const successResponse = h.response({
      status: 'success',
      message: 'Note has successfully added',
      data: {
        noteId: id,
      },
    });

    successResponse.code(201);
    return successResponse;
  }

  const failedResponse = h.response({
    status: 'failed',
    message: 'Failed to added note',
  });

  failedResponse.code(500);
  return failedResponse;
};

export default addNote;
