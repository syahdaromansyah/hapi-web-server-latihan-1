import notes from '../../database/notes.mjs';

const getNotes = () => ({
  status: 'success',
  data: {
    notes,
  },
});

export default getNotes;
