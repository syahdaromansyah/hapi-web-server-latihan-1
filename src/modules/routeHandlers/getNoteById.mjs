import notes from '../../database/notes.mjs';

const getNoteById = (request, h) => {
  const { id } = request.params;
  const noteData = notes.filter((note) => note.id === id)[0];
  const noteExist = noteData !== undefined;

  if (noteExist) {
    return {
      status: 'success',
      data: {
        noteData,
      },
    };
  }

  const failedResponse = h.response({
    status: 'failed',
    message: 'Note is not exist',
  });

  failedResponse.code(404);
  return failedResponse;
};

export default getNoteById;
