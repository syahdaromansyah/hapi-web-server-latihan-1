import notes from '../../database/notes.mjs';

const deleteNoteById = (request, h) => {
  const { id } = request.params;
  const noteIdx = notes.findIndex((note) => note.id === id);
  const noteIsExist = noteIdx !== -1;

  if (noteIsExist) {
    notes.splice(noteIdx, 1);

    const successResponse = h.response({
      status: 'success',
      message: 'Note has successfully deleted',
    });

    successResponse.code(200);
    return successResponse;
  }

  const failedResponse = h.response({
    status: 'failed',
    message: 'Failed to deleting note. Note is not exist',
  });

  failedResponse.code(404);
  return failedResponse;
};

export default deleteNoteById;
