import notes from '../../database/notes.mjs';

const updateNoteById = (request, h) => {
  const { id } = request.params;
  const { title, tags, body } = request.payload;
  const updateAt = new Date().toISOString();
  const noteIdx = notes.findIndex((note) => note.id === id);
  const noteIsExist = noteIdx !== -1;

  if (noteIsExist) {
    notes[noteIdx] = {
      ...notes[noteIdx],
      title,
      tags,
      body,
      updateAt,
    };

    const successResponse = h.response({
      status: 'success',
      message: 'Note has successfully updated',
    });

    successResponse.code(200);
    return successResponse;
  }

  const failedResponse = h.response({
    status: 'failed',
    message: 'Failed to updating note. Note is not exist',
  });

  failedResponse.code(404);
  return failedResponse;
};

export default updateNoteById;
