import addNote from './routeHandlers/addNote.mjs';
import getNoteById from './routeHandlers/getNoteById.mjs';
import getNotes from './routeHandlers/getNotes.mjs';
import updateNoteById from './routeHandlers/updateNoteById.mjs';
import deleteNoteById from './routeHandlers/deleteNoteById.mjs';

const routes = [
  {
    method: 'POST',
    path: '/notes',
    handler: addNote,
  },
  {
    method: 'GET',
    path: '/notes',
    handler: getNotes,
  },
  {
    method: 'GET',
    path: '/notes/{id}',
    handler: getNoteById,
  },
  {
    method: 'PUT',
    path: '/notes/{id}',
    handler: updateNoteById,
  },
  {
    method: 'DELETE',
    path: '/notes/{id}',
    handler: deleteNoteById,
  },
];

export default routes;
